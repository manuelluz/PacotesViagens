﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacotesViagens.Modelos
{
    public class Pacotes
    {

        #region atributos

        private int _Id;
        private string _Descricao;
        private double _Preco;

        #endregion

        #region propriedades
        public int Id { get; set; }

        public String Descricao { get; set; }

        public double Preco { get; set; }

        #endregion

        #region Método Genéricos
        public override string ToString()
        {
            return string.Format("Id Pacotes:{0} - Descrição:{1} - Preço:{2}", Id, Descricao, Preco);
        }
        #endregion
        
    }
}
