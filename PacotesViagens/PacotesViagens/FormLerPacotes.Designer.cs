﻿namespace PacotesViagens
{
    partial class FormLerPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridViewLer = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrição = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Preço = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonSair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewLer)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewLer
            // 
            this.DataGridViewLer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewLer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Descrição,
            this.Preço});
            this.DataGridViewLer.Location = new System.Drawing.Point(13, 13);
            this.DataGridViewLer.Name = "DataGridViewLer";
            this.DataGridViewLer.Size = new System.Drawing.Size(376, 338);
            this.DataGridViewLer.TabIndex = 0;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Descrição
            // 
            this.Descrição.HeaderText = "Descrição";
            this.Descrição.Name = "Descrição";
            this.Descrição.ReadOnly = true;
            // 
            // Preço
            // 
            this.Preço.HeaderText = "Preço";
            this.Preço.Name = "Preço";
            this.Preço.ReadOnly = true;
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackgroundImage = global::PacotesViagens.Properties.Resources.if_Close_Icon_1398919;
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonSair.Location = new System.Drawing.Point(392, 374);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(27, 28);
            this.ButtonSair.TabIndex = 1;
            this.ButtonSair.UseVisualStyleBackColor = true;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // FormLerPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 414);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.DataGridViewLer);
            this.Name = "FormLerPacotes";
            this.Text = "FormLerPacotes";
            this.Load += new System.EventHandler(this.FormLerPacotes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewLer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewLer;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrição;
        private System.Windows.Forms.DataGridViewTextBoxColumn Preço;
        private System.Windows.Forms.Button ButtonSair;
    }
}