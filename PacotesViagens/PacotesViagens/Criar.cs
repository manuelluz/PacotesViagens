﻿using PacotesViagens.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacotesViagens
{
    public partial class FromCriar : Form
    {

        private List<Pacotes> ListaDePacotes = new List<Pacotes>();


        public FromCriar(List<Pacotes> ListaDePacotes)
        {
            InitializeComponent();
            this.ListaDePacotes = ListaDePacotes;
        }

        private void Criar_Load(object sender, EventArgs e)
        {

        }

        private void ButtonFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonGravar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxDescricao.Text))
            {
                MessageBox.Show("Tem que inserir a descrição do pacote de viagens.");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxPreco.Text))
            {
                MessageBox.Show("Tem que inserir o preço do pacote de viagens.");
                return;
            }

            var pacotes = new Pacotes
            {
                Id = GeraIdPacote(),
                Descricao = TextBoxDescricao.Text,
                Preco = Convert.ToDouble(TextBoxPreco.Text)
            };

            ListaDePacotes.Add(pacotes);
            MessageBox.Show("Novo pacote de viagem inserido com sucesso");
            Close();
        }
        private int GeraIdPacote()
        {
            return ListaDePacotes[ListaDePacotes.Count - 1].Id + 1;
        }


    }
}

