﻿using PacotesViagens.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PacotesViagens
{      

    public partial class FormLerPacotes : Form
    {

        private List<Pacotes> ListaDePacotes;

        public FormLerPacotes(List<Pacotes> listaPacotes)
        {
            InitializeComponent();            
            this.ListaDePacotes = listaPacotes;
        }

        public FormLerPacotes()
        {
        }

        private void FormLerPacotes_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < ListaDePacotes.Count; i++)
            {
                DataGridViewLer.Rows.Add(ListaDePacotes[i].Id, ListaDePacotes[i].Descricao, ListaDePacotes[i].Preco);
                DataGridViewLer.AllowUserToAddRows = false;
            }
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
