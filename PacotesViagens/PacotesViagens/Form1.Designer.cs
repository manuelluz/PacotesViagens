﻿namespace PacotesViagens
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aBOUTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.ButtonEditar = new System.Windows.Forms.Button();
            this.ButtonProcurar = new System.Windows.Forms.Button();
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBOUTToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(335, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aBOUTToolStripMenuItem
            // 
            this.aBOUTToolStripMenuItem.Name = "aBOUTToolStripMenuItem";
            this.aBOUTToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.aBOUTToolStripMenuItem.Text = "ABOUT";
            this.aBOUTToolStripMenuItem.Click += new System.EventHandler(this.aBOUTToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Criar";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(226, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Procurar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(226, 255);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Apagar";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Editar";
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.BackgroundImage = global::PacotesViagens.Properties.Resources.if_Streamline_70_185090;
            this.ButtonApagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonApagar.Location = new System.Drawing.Point(226, 177);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(75, 71);
            this.ButtonApagar.TabIndex = 3;
            this.ButtonApagar.UseVisualStyleBackColor = true;
            // 
            // ButtonEditar
            // 
            this.ButtonEditar.BackgroundImage = global::PacotesViagens.Properties.Resources.if_edit_103539;
            this.ButtonEditar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonEditar.Location = new System.Drawing.Point(29, 177);
            this.ButtonEditar.Name = "ButtonEditar";
            this.ButtonEditar.Size = new System.Drawing.Size(75, 71);
            this.ButtonEditar.TabIndex = 2;
            this.ButtonEditar.UseVisualStyleBackColor = true;
            // 
            // ButtonProcurar
            // 
            this.ButtonProcurar.BackgroundImage = global::PacotesViagens.Properties.Resources.if_67_111124;
            this.ButtonProcurar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonProcurar.Location = new System.Drawing.Point(226, 44);
            this.ButtonProcurar.Name = "ButtonProcurar";
            this.ButtonProcurar.Size = new System.Drawing.Size(75, 71);
            this.ButtonProcurar.TabIndex = 1;
            this.ButtonProcurar.UseVisualStyleBackColor = true;
            this.ButtonProcurar.Click += new System.EventHandler(this.ButtonProcurar_Click);
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.BackgroundImage = global::PacotesViagens.Properties.Resources.if_199_CircledPlus_183316;
            this.ButtonCriar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonCriar.Location = new System.Drawing.Point(29, 44);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(75, 71);
            this.ButtonCriar.TabIndex = 0;
            this.ButtonCriar.UseVisualStyleBackColor = true;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::PacotesViagens.Properties.Resources.if_Close_Icon_1398919;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(290, 297);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 40);
            this.button1.TabIndex = 9;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 349);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonApagar);
            this.Controls.Add(this.ButtonEditar);
            this.Controls.Add(this.ButtonProcurar);
            this.Controls.Add(this.ButtonCriar);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "PACOTES VIAGENS";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Button ButtonProcurar;
        private System.Windows.Forms.Button ButtonEditar;
        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aBOUTToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
    }
}

