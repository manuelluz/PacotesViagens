﻿namespace PacotesViagens
{
    partial class FromCriar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.TextBoxPreco = new System.Windows.Forms.TextBox();
            this.TextBoxIdPacote = new System.Windows.Forms.TextBox();
            this.ButtonFechar = new System.Windows.Forms.Button();
            this.ButtonGravar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Pacote";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descrição";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Preço";
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Location = new System.Drawing.Point(119, 66);
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.Size = new System.Drawing.Size(100, 20);
            this.TextBoxDescricao.TabIndex = 3;
            // 
            // TextBoxPreco
            // 
            this.TextBoxPreco.Location = new System.Drawing.Point(119, 101);
            this.TextBoxPreco.Name = "TextBoxPreco";
            this.TextBoxPreco.Size = new System.Drawing.Size(100, 20);
            this.TextBoxPreco.TabIndex = 4;
            // 
            // TextBoxIdPacote
            // 
            this.TextBoxIdPacote.Location = new System.Drawing.Point(119, 31);
            this.TextBoxIdPacote.Name = "TextBoxIdPacote";
            this.TextBoxIdPacote.ReadOnly = true;
            this.TextBoxIdPacote.Size = new System.Drawing.Size(100, 20);
            this.TextBoxIdPacote.TabIndex = 5;
            // 
            // ButtonFechar
            // 
            this.ButtonFechar.BackgroundImage = global::PacotesViagens.Properties.Resources.if_Close_Icon_1398919;
            this.ButtonFechar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonFechar.Location = new System.Drawing.Point(95, 140);
            this.ButtonFechar.Name = "ButtonFechar";
            this.ButtonFechar.Size = new System.Drawing.Size(61, 58);
            this.ButtonFechar.TabIndex = 7;
            this.ButtonFechar.UseVisualStyleBackColor = true;
            this.ButtonFechar.Click += new System.EventHandler(this.ButtonFechar_Click);
            // 
            // ButtonGravar
            // 
            this.ButtonGravar.BackgroundImage = global::PacotesViagens.Properties.Resources.if_Tick_Mark_1398911;
            this.ButtonGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonGravar.Location = new System.Drawing.Point(170, 140);
            this.ButtonGravar.Name = "ButtonGravar";
            this.ButtonGravar.Size = new System.Drawing.Size(61, 58);
            this.ButtonGravar.TabIndex = 6;
            this.ButtonGravar.UseVisualStyleBackColor = true;
            this.ButtonGravar.Click += new System.EventHandler(this.ButtonGravar_Click);
            // 
            // FromCriar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(243, 210);
            this.Controls.Add(this.ButtonFechar);
            this.Controls.Add(this.ButtonGravar);
            this.Controls.Add(this.TextBoxIdPacote);
            this.Controls.Add(this.TextBoxPreco);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FromCriar";
            this.Text = "Criar";
            this.Load += new System.EventHandler(this.Criar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.TextBox TextBoxPreco;
        private System.Windows.Forms.TextBox TextBoxIdPacote;
        private System.Windows.Forms.Button ButtonGravar;
        private System.Windows.Forms.Button ButtonFechar;
    }
}