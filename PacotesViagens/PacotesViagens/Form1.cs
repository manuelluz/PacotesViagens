﻿using PacotesViagens.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacotesViagens
{
    public partial class Form1 : Form
    {
        private List<Pacotes> ListaDePacotes;
        private FromCriar formularioCriar;
        
        FormLerPacotes l = new FormLerPacotes();


        public Form1()
        {
            InitializeComponent();

            ListaDePacotes = new List<Pacotes>();
                        
            ControlBox = false;

            ListaDePacotes.Add(new Pacotes { Id = 1, Descricao = "Barcelona", Preco = 800.5 });
            ListaDePacotes.Add(new Pacotes { Id = 2, Descricao = "NewYork", Preco = 1087.3 });
            ListaDePacotes.Add(new Pacotes { Id = 3, Descricao = "Paris", Preco = 732.4 });
            ListaDePacotes.Add(new Pacotes { Id = 4, Descricao = "Sydney", Preco = 2187.5 });
            ListaDePacotes.Add(new Pacotes { Id = 5, Descricao = "Londres", Preco = 567.2 });
            ListaDePacotes.Add(new Pacotes { Id = 6, Descricao = "Açores", Preco = 378.5 });
            ListaDePacotes.Add(new Pacotes { Id = 7, Descricao = "Faro", Preco = 90 });

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonCriar_Click(object sender, EventArgs e)
        {
            formularioCriar = new FromCriar(ListaDePacotes);
            formularioCriar.Show();
        }

        private void aBOUTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pacotes de Viagens\n\nManuel Luz\n\nVersao: 1.0.1\n\n10/10/2017");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonProcurar_Click(object sender, EventArgs e)
        {
            l = new FormLerPacotes(ListaDePacotes);
            l.Show();
        }
    }
}
